from django.forms import ModelForm
from .models import Cedula

class CedulaForm(ModelForm):
    class Meta:
        model = Cedula
        fields = ['adelante', 'atras']