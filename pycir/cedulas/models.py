from django.db import models

# Create your models here.

class Cedula(models.Model):
    nombre = models.CharField(max_length=255, default='')
    apellido = models.CharField(max_length=255, default='')
    adelante = models.ImageField(upload_to='adelante')
    atras = models.ImageField(upload_to='atras')
    documento_paraguayo = models.BooleanField(default=False)
    fecha_nacimiento = models.DateField(blank=True, null=True)
    vencimiento = models.DateField(blank=True, null=True)

    def __str__(self):
        return str(self.nombre) + " " + str(self.apellido)