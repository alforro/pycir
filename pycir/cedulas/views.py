from django.shortcuts import render
from django.http import HttpResponseRedirect

# Create your views here.

from .forms import CedulaForm
from passporteye.mrz.image import MRZPipeline
import json

def home(request):
    if request.method == 'POST':
        form = CedulaForm(request.POST, request.FILES)
        if form.is_valid():
            print (request.FILES)
            atras = request.FILES['atras']
            atras = atras.read()
            p = MRZPipeline(atras)
            mrz = p.result
            context = mrz.to_dict() 
            form.save()
            return render(request, 'results.html', context)
            #return HttpResponseRedirect('/datos', {context})
    else:
        form = CedulaForm()
    return render(request, 'home.html', {'form': form})

def restultados(request):
    return render(request,'results.html')