from django.apps import AppConfig


class CedulasConfig(AppConfig):
    name = 'cedulas'
