from django.urls import path
from . import views

urlpatterns = [
    path('datos', views.restultados),
    path('', views.home)
]
